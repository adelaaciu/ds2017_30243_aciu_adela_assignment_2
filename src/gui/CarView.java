package gui;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import common.entities.Car;
import common.serviceinterface.IComputeService;

import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import service.CarService;

public class CarView extends JFrame{
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JButton priceButton;
    private  JButton taxButton;
    private JTextField yearField;
    private JTextField engineCapacityField;
    private JTextField priceField;
    private IComputeService carService;

    public  CarView(IComputeService carService){
        this.carService =  carService;
        creteContent();
    }

    private void creteContent() {
        setTitle("Car taxes");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);

        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblYear = new JLabel("Year");
        lblYear.setBounds(10, 36, 60, 14);
        contentPane.add(lblYear);

        JLabel lblEngine = new JLabel("Engine capacity");
        lblEngine.setBounds(10, 61, 60, 14);
        contentPane.add(lblEngine);

        JLabel lblPrice = new JLabel("Price");
        lblPrice.setBounds(10, 86, 46, 14);
        contentPane.add(lblPrice);

        yearField = new JTextField();
        yearField.setBounds(80, 33, 86, 20);
        contentPane.add(yearField);
        yearField.setColumns(10);

        engineCapacityField = new JTextField();
        engineCapacityField.setBounds(80, 58, 86, 20);
        contentPane.add(engineCapacityField);
        engineCapacityField.setColumns(10);

        priceField = new JTextField();
        priceField.setBounds(80, 83, 86, 20);
        contentPane.add(priceField);
        priceField.setColumns(10);

        priceButton = new JButton("Get price");
        priceButton.setBounds(10, 132, 89, 23);
        contentPane.add(priceButton);


        taxButton = new JButton("Get tax");
        taxButton.setBounds(200, 132, 89, 23);
        contentPane.add(taxButton);

        addListener();
    }

    private void addListener(){
        priceButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    float price = computePrice();
                    JOptionPane.showMessageDialog(null, "Price is: " + price);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });


        taxButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    double tax = computeTax();
                    JOptionPane.showMessageDialog(null, "Tax is: " + tax);
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    public float computePrice() throws RemoteException {
        int year = Integer.parseInt(yearField.getText());
        int engineCapacity = Integer.parseInt(engineCapacityField.getText());
        float price = Float.valueOf(priceField.getText());

        Car c = new Car(year, engineCapacity, price);
        return carService.computePrice(c);
    }

    public double computeTax() throws RemoteException {
        int year = Integer.parseInt(yearField.getText());
        int engineCapacity = Integer.parseInt(engineCapacityField.getText());
        float price = Float.valueOf(priceField.getText());
        Car c = new Car(year, engineCapacity, price);

        return carService.computeTax(c);
    }
}
