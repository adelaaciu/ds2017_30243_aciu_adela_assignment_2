package client;

import common.serviceinterface.IComputeService;
import gui.CarView;

import java.rmi.Naming;
public class Client {
    private Client() {}

    public static void main(String[] args) {
        try {
            IComputeService computeService;
            computeService = (IComputeService) Naming.lookup("rmi://localhost:1099/testRMI");
            CarView view = new CarView(computeService);
            view.setVisible(true);
            System.out.println("Remote invoked");
        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
