package service;

import common.entities.Car;
import common.serviceinterface.IComputeService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class CarService extends UnicastRemoteObject implements IComputeService {

    public CarService() throws RemoteException{}

    @Override
    public float computePrice(Car c) throws RemoteException {
        float sellingPrice = c.getPrice() - (c.getPrice() / 7) * (2015 - c.getYear());
        return sellingPrice;
    }

    @Override
    public double computeTax(Car c) throws RemoteException{
        if (c.getEngineCapacity() <= 0) {
            throw new IllegalArgumentException("Engine capacity must be positive.");
        }
        int sum = 8;
        if(c.getEngineCapacity() > 1601) sum = 18;
        if(c.getEngineCapacity() > 2001) sum = 72;
        if(c.getEngineCapacity() > 2601) sum = 144;
        if(c.getEngineCapacity() > 3001) sum = 290;
        return c.getEngineCapacity() / 200.0 * sum;
    }
}
