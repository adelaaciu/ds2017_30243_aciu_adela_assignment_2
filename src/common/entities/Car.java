package common.entities;


import java.io.Serializable;

public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    private int year;
    private int engineCapacity;
    private float price;

    public Car() {
    }

    public Car(int year, int engineCapacity, float price) {
        this.year = year;
        this.engineCapacity = engineCapacity;
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }


    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Car car = (Car) o;

        if (year != car.year) return false;
        if (engineCapacity != car.engineCapacity) return false;
        return Float.compare(car.price, price) == 0;
    }

    @Override
    public int hashCode() {
        int result = year;
        result = 31 * result + engineCapacity;
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Car{" +
                "year=" + year +
                ", engineCapacity=" + engineCapacity +
                ", price=" + price +
                '}';
    }
}
