package common.serviceinterface;

import common.entities.Car;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IComputeService extends Remote {

    float computePrice(Car c) throws RemoteException;

    double computeTax(Car c) throws RemoteException;
}
