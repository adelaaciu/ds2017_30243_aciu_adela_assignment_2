package server;

import common.serviceinterface.IComputeService;
import service.CarService;

import java.rmi.Naming;
public class Server{
    public Server() {
    }

    public static void main(String[] args) {

        try {
            IComputeService computeService = new CarService();
            java.rmi.registry.LocateRegistry.createRegistry(1099);
            Naming.rebind("rmi://localhost:1099/testRMI", computeService);
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
